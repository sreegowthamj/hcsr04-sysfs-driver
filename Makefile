MODULE = hcsr_pf_driver
obj-m += $(MODULE).o platform_device.o
APP = hcsr04_driver_test
check_defined = \
    $(strip $(foreach 1,$1, \
         $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
     $(if $(value $1),, \
       $(error Undefined $1$(if $2, ($2))))

$(call check_defined, CROSS_COMPILE, SDKTARGETSYSROOT, ARCH)

# ###################################################################

all: compile

compile:
	@make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) modules

	@$(CROSS_COMPILE)gcc -o $(APP) -Wall --sysroot=$(SDKTARGETSYSROOT) -lm -lpthread hcsr04_test.c
clean:
	@make -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) clean 1> /dev/null
	rm -rf $(APP) 

flash: compile
	scp $(MODULE).ko platform_device.ko $(APP) test.sh  root@192.168.1.5:/home/root/ 

# KDIR := /lib/modules/$(shell uname -r)/build
# PWD := $(shell pwd)

# CC := $(CROSS_COMPILE)gcc

# all:
# 	$(MAKE) -C $(KDIR) M=${shell pwd} modules

# clean:
# 	-$(MAKE) -C $(KDIR) M=${shell pwd} clean || true
# 	-rm *.o *.ko *.mod.{c,o} modules.order Module.symvers || true
