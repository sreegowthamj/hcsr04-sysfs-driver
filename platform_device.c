#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>

#include "platform_device.h"

#define CLASS_NAME "HCSR"
#define PLATFORM_DRIVER "hcsr_pf_driver"

static void hcsrdevice_release(struct device *dev)
{
	pr_info("nothing to do");
}

static hcsr04_device_t hcsr_device0 = {
	.name = DEVICE_NAME1,
	.dev_no = 1,
	.pf_dev = {.name = DEVICE_NAME1,
		    .id = -1,
		    .dev = {
			    .release = hcsrdevice_release,
		    }}};

static hcsr04_device_t hcsr_device1 = {
	.name = DEVICE_NAME2,
	.dev_no = 2,
	.pf_dev = {.name = DEVICE_NAME2,
		    .id = -1,
		    .dev = {
			    .release = hcsrdevice_release,
		    }}};

/**
 * Register the device when module is loaded
 */

static int p_device_init(void)
{
	int ret = 0;

	/* Register the device */
	platform_device_register(&hcsr_device0.pf_dev);

	pr_notice("Platform device 1 is registered in init \n");

	platform_device_register(&hcsr_device1.pf_dev);

	pr_notice("Platform device 2 is registered in init \n");

	return ret;
}

static void p_device_exit(void)
{
	platform_device_unregister(&hcsr_device0.pf_dev);

	platform_device_unregister(&hcsr_device1.pf_dev);

	pr_notice("Unregister the platform device done\n");
}

module_init(p_device_init);
module_exit(p_device_exit);
MODULE_LICENSE("GPL");
