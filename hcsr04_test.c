#include "hc-sr04_ioctl.h"

#include <fcntl.h>     /* open */
#include <unistd.h>    /* exit */
#include <sys/ioctl.h> /* ioctl */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#define DEVICE_FILE_NAME "/dev/HCSR_2"

typedef enum ioctl_op { ENUM_SET_PARAMETERS, ENUM_CONFIG_PINS } ioctl_op_t;

typedef struct read_data {
	uint64_t time_stamp;
	int distance;
} read_data_t;


void ioctl_set_msg(int file_desc, char *msg, ioctl_op_t m_ioctl_op)
{
	int ret_val;

	switch (m_ioctl_op) {
	case ENUM_SET_PARAMETERS: {
		ret_val = ioctl(file_desc, SET_PARAMETERS, msg);
		break;
	}
	case ENUM_CONFIG_PINS: {
		ret_val = ioctl(file_desc, CONFIG_PINS, msg);
		break;
	}
	default:
		break;
	}


	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
	}
}


int main(int argc, char **argv)
{
	int file_desc, ret_val;
	char *msg;
	sampling_params_t s1;
	config_pins_t c1;
	int index = 0;
	s1.num_samples = 21;
	s1.sampling_period = 42;
	c1.echo_pin = 4;
	c1.trigger_pin = 5;
	int *clear_per_device_buff = 0;
	int val = 2;
	int *read_buf;

	while (--argc != 0) {
		index += 1;
		if (strcmp(argv[index], "test_ioctl") == 0) {
			break;
		}
	}

	file_desc = open(DEVICE_FILE_NAME, O_RDWR);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		exit(-1);
	}

	/* Setting SET_PARAMETERS ioctl **************************************/
	msg = (char *)malloc(sizeof(sampling_params_t));
	printf("doing memcpy \n");
	memcpy(msg, &s1, sizeof(s1));
	printf("memcpy success \n");
	ioctl_set_msg(file_desc, msg, ENUM_SET_PARAMETERS);
	printf("ioctl success \n");

	/* Setting CONFIG_PINS IOCTL *****************************************/
	msg = (char *)malloc(sizeof(config_pins_t));
	printf("doing memcpy \n");
	memcpy(msg, &c1, sizeof(s1));
	printf("memcpy success \n");
	ioctl_set_msg(file_desc, msg, ENUM_CONFIG_PINS);
	printf("ioctl success \n");

	/* Write Operation: Trigger  measurement *****************************/
	clear_per_device_buff = (int *)malloc(sizeof(int));
	clear_per_device_buff = &val;
	ret_val = write(file_desc, &clear_per_device_buff,
			sizeof(clear_per_device_buff));
	if (ret_val < 0)
		printf("Write operation failed, value = %d \n", ret_val);
	/* Read operation: read the average value and Timestap*****************/

	sleep(10);
	read_buf = malloc(sizeof(int));
	memset(read_buf, 0, sizeof(int));
	ret_val = read(file_desc, read_buf, sizeof(read_buf));
	if (ret_val < 0)
		printf("Read failed, ret_val = %d \n", ret_val);
	else {
		printf("Read result : avg value = %d \n", *read_buf);
	}

	close(file_desc);

	return 0;
}
