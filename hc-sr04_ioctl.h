/**
 *   \file hc-sr04_ioctl.h
 *   \brief IOCTL definitions for setting
	       a) sampling parameters
	       b) configuration pins
 *
 *  Detailed description
 *
 */


#ifndef _HC_SR04_IOCTL_H_
#define _HC_SR04_IOCTL_H_


#include <linux/ioctl.h>

/* Major Number is fixed for misc devices to 10  */
#define MAJOR_NUM 10

/* Datatype for SET_PARAMETERS  ioctl */

typedef struct sampling_params {
	int num_samples;
	int sampling_period;
} sampling_params_t;

/* Datatype for CONFIG_PINS ioctl */
typedef struct config_pins {
	int echo_pin;
	int trigger_pin;
} config_pins_t;


#define SET_PARAMETERS _IOW(MAJOR_NUM, 0, char *)

#define CONFIG_PINS _IOW(MAJOR_NUM, 1, config_pins_t *)

#endif /* _HC_SR04_IOCTL_H_ */
