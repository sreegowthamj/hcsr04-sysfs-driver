#! /bin/bash

insmod hcsr_pf_driver.ko
insmod platform_device.ko

echo 4 > /sys/class/HCSR/HCSR1/trigger
echo 5 > /sys/class/HCSR/HCSR1/echo
echo 60 > /sys/class/HCSR/HCSR1/sampling_period
echo 25 > /sys/class/HCSR/HCSR1/number_samples
echo 1 > /sys/class/HCSR/HCSR1/enable &
sleep 1
echo 0 > /sys/class/HCSR/HCSR1/enable &
sleep 1
echo 1 > /sys/class/HCSR/HCSR1/enable
echo "The average distance calculated in cm is"
cat  /sys/class/HCSR/HCSR1/distance
