#ifndef IO_SHIELD_TO_GPIO_MAPPINGS_H
#define IO_SHIELD_TO_GPIO_MAPPINGS_H

#define IO_SHILED_PINS 0
#define LINUX_PIN 1
#define LEVEL_SHIFTER_PIN 2
#define PIN_MUX1 3
#define PIN_MUX2 4

#define OUT_PIN 0
#define IN_PIN 1

int gpio_table [14][5] = {{0, 11, 32, -1, -1},
                      {1, 12, 28, 45, -1},
                      {2, 13, 34, 77, -1},
                      {3, 14, 16, 76, 64},
                      {4, 6, 36, -1, -1},
                      {5, 0, 18, 66, -1},
                      {6, 1, 20, 68, -1},
                      {7, 38, -1, -1, -1},
                      {8, 40, -1, -1, -1},
                      {9, 4, 22, 70, -1},
                      {10, 10, 26, 74, -1},
                      {11, 5, 24, 44, 72},
                      {12, 15, 42, -1, -1},
                      {13, 7, 30, 46, -1}};

/* Trigger is output for Gallileo*/
/* level shifter value is 0 and direction is out for all trigger pins */
/* The below table contains only pin_mux_1 and pin_mux_2 values where applicable */
int pin_mux_table [14][2] = {{-1, -1},
                              {0, -1},
                              {0, -1}, //IO2
                              {0, 0},
                              {-1, -1},
                              {0, -1}, // IO5
                              {0, -1},
                              {-1, -1},
                              {-1, -1},
                              {0, -1},
                              {0, -1}, //IO10
                              {0, 0},
                              {-1 ,-1},
                              {0, -1}};

#endif /* IO_SHIELD_TO_GPIO_MAPPINGS_H */
