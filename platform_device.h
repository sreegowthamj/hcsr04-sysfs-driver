#ifndef __PLATFORM_DEVICE__H__

#define __PLATFORM_DEVICE__H__

#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/list.h>
#include <linux/semaphore.h>
#include <linux/unistd.h>
#include <linux/gpio.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include "hc-sr04_ioctl.h"

#define SIZE_OF_BUF 5
#define GPIO_INT_NAME "gpio_int"
#define CLASS_NAME "HCSR"
#define DRIVER_NAME "hcsr_pf_driver"
#define DEVICE_NAME1 "HCSR1"
#define DEVICE_NAME2 "HCSR2"


typedef struct hcsr04_device {
	char *name;
	int dev_no;
	struct platform_device pf_dev;
} hcsr04_device_t;

typedef enum write_status {
	WRITING,
	NOT_WRITING,
	INVALID_STATUS
} write_status_t;


typedef struct circular_buffer {
	void *buffer;     // data buffer
	void *buffer_end; // end of data buffer
	size_t capacity;  // maximum number of items in the buffer
	size_t count;     // number of items in the buffer
	size_t sz;	// size of each item in the buffer
	void *head;       // pointer to head
	void *tail;       // pointer to tail
} circular_buffer;


/* Creating per device structure type */
typedef struct htsr04_data {
	/* Misc. device data struct */
	struct miscdevice htsr04_device;
	config_pins_t m_config_pins; /* set through ioctl CONFIG_PINS */
	hcsr04_device_t *hcsr_dev_p;
	char *dev_name;
	/* Set through ioctl SET_PARAMETERS */
	sampling_params_t m_sampling_params;
	int *distances_mm;
	int distances_count;
	write_status_t m_write_status;
	circular_buffer *cb;
	spinlock_t m_Lock;
	int trigger_pin;
	int enable; /* 1 to start measurement, 0 to stop measurement */
	int recent_distance;
	struct list_head device_entry;
} htsr04_data_t;


#endif /* __PLATFORM_DEVICE__H__ */
