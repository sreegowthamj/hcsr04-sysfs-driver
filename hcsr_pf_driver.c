#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/gpio.h> // Required for the GPIO functions
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <linux/interrupt.h> // Required for the IRQ code
#include <linux/delay.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>

#include "hc-sr04_ioctl.h"
#include "platform_device.h"
#include "io_shield_to_gpio_mappings.h"

#define EXTRA_DEBUG 1
#define BUFSIZE 9
#define GPIO_NAME "gpio_int_hcsr04"
#define HCSR_RISING_EDGE_INTERRUPT 0
#define HCSR_FALLING_EDGE_INTERRUPT 1
#define CIRCULAR_BUFFER_SIZE 5

static DEFINE_MUTEX(enable_lock);
static DEFINE_MUTEX(distance_lock);

typedef enum create_class { CREATE_CLASS, CLASS_CREATED } create_class_t;
create_class_t m_create_class = CREATE_CLASS;
static struct class *p_hcsr_class;
static struct device *p_hcsr_device;
static dev_t pf_devt = 1;
static LIST_HEAD(device_list);
static int n = 5;
struct miscdevice *htsr04_dev;
module_param(n, int, 0);
MODULE_PARM_DESC(n, "Number of devices");

typedef struct output_data {
	uint64_t time_stamp;
	int distance;
} output_data_t;

static int minor_num_base = 1;

htsr04_data_t *htsr04_datap;
/* TODO: have logic to free this irq_id */
int irq_id;
int g_interrupt_type = HCSR_RISING_EDGE_INTERRUPT;

static const struct platform_device_id pdev_id_table[] = {
	{DEVICE_NAME1, 0},
	{DEVICE_NAME2, 0},
};
static __inline__ unsigned long long get_tsc(void)

{
	unsigned long long int x;
	__asm__ volatile(".byte 0x0f, 0x31" : "=A"(x));
	return x;
}

uint64_t tsc_start = 0, tsc_end = 0;

void cb_init(circular_buffer *cb, size_t capacity, size_t sz)
{
	pr_info("line1");
	cb->buffer = kmalloc(capacity * sz, GFP_KERNEL);
	if (cb->buffer == NULL)
		// handle error
		cb->buffer_end = (char *)cb->buffer + capacity * sz;
	pr_info("line2");
	cb->capacity = capacity;
	pr_info("line3");
	cb->count = 0;
	pr_info("line4");
	cb->sz = sz;
	pr_info("line5");
	cb->head = cb->buffer;
	pr_info("line6");
	cb->tail = cb->buffer;
}

void cb_free(circular_buffer *cb)
{
	kfree(cb->buffer);
	// clear out other fields too, just to be safe
}

void cb_push_back(circular_buffer *cb, const void *item)
{
	if (cb->count == cb->capacity) {
		// handle error
	}
	memcpy(cb->head, item, cb->sz);
	cb->head = (char *)cb->head + cb->sz;
	if (cb->head == cb->buffer_end)
		cb->head = cb->buffer;
	cb->count++;
}

void cb_pop_front(circular_buffer *cb, void *item)
{
	if (cb->count == 0) {
		// handle error
	}
	memcpy(item, cb->tail, cb->sz);
	cb->tail = (char *)cb->tail + cb->sz;
	if (cb->tail == cb->buffer_end)
		cb->tail = cb->buffer;
	cb->count--;
}


void trigger_pulse(htsr04_data_t *dev_datap)
{
	int step = 0, delay = 15, i = 0, avg = 0, temp = 0;
	output_data_t x;
	/* TODO: Set flag to indiacate that measurement has started */
	/* Trigger the Sensor to high for 10 us*/
	int gpio_pin =
		gpio_table[dev_datap->m_config_pins.trigger_pin][LINUX_PIN];

	while (dev_datap->distances_count
	       < dev_datap->m_sampling_params.num_samples + 2) {
		mutex_lock(&enable_lock);
		temp = dev_datap->enable;
		mutex_unlock(&enable_lock);
		if (temp == 0) {
			pr_notice(
				"Canceling the data collection as enable = 0");
			return;
		}
		// gpio_set_value_cansleep(0, 0);

		delay += step;
		pr_info("Trigger_pulse: pulse width = %d \n", delay);
		gpio_set_value_cansleep(gpio_pin, 1);
		udelay(delay);
		/* set the pin to low */
		gpio_set_value_cansleep(gpio_pin, 0);
		msleep(60);
	}
	/* Of the m+2 samples, finding the mean of m samples */
	/* TODO: put some lock here */
	for (i = 1; i <= dev_datap->distances_count - 2; i++) {
		pr_info(" Distance = %d, i = %d", dev_datap->distances_mm[i],
			i);
		avg = avg + dev_datap->distances_mm[i];
	}
	avg = (int)(avg / dev_datap->m_sampling_params.num_samples);
	pr_warn("average distance = %d \n", avg);
	mutex_lock(&distance_lock);
	dev_datap->recent_distance = avg;
	mutex_unlock(&distance_lock);
	x.distance = avg;
	x.time_stamp = get_tsc();
	cb_push_back(dev_datap->cb, &x);
	dev_datap->distances_count = 0;
}

static irqreturn_t r_irq_handler(int irq, void *datap)
{
	int distance;
	htsr04_data_t *dev_datap = (htsr04_data_t *)datap;

	pr_info("r_irq_handler: for %s ",
		((htsr04_data_t *)dev_datap)->htsr04_device.name);
	if (g_interrupt_type == HCSR_RISING_EDGE_INTERRUPT) {
		tsc_start = get_tsc();
		pr_info("Rising edge Interrupt\n");
		irq_set_irq_type(irq, IRQ_TYPE_EDGE_FALLING);
		g_interrupt_type = HCSR_FALLING_EDGE_INTERRUPT;
	} else if (g_interrupt_type == HCSR_FALLING_EDGE_INTERRUPT) {
		tsc_end = get_tsc();
		pr_info("Falling edge Interrupt \n");
		irq_set_irq_type(irq, IRQ_TYPE_EDGE_RISING);
		g_interrupt_type = HCSR_RISING_EDGE_INTERRUPT;
		if ((tsc_start != 0) && (tsc_end > tsc_start)) {
			distance = (int)(tsc_end - tsc_start) / (1392000);
			pr_info("distance = %d cm \n", distance);
			if (dev_datap->distances_count
			    < 2 + dev_datap->m_sampling_params.num_samples) {
				/* mutex start */
				dev_datap->distances_mm
					[dev_datap->distances_count] = distance;
				dev_datap->distances_count += 1;
				/* mutex end */
			}
		}
	}
	pr_info("Returning from IRQ_handler");
	return IRQ_HANDLED;
}

static int export_gpio_pin(int io_pin, int io_pin_dir)
{
	int ret_val = 0, gpio_pin;
	gpio_pin = gpio_table[io_pin][LINUX_PIN];
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("LINUX_PIN allocation failed for GPIO pin = %d \n",
				gpio_pin);
			return -1;
		}
		if (io_pin_dir == OUT_PIN) {
			ret_val = gpio_direction_output(gpio_pin, 0);
			gpio_set_value_cansleep(gpio_pin, 0);
		} else if (io_pin_dir == IN_PIN) {
			ret_val = gpio_direction_input(gpio_pin);
		}
		if (ret_val != 0) {
			pr_warn("LINUX_PIN direction set failed for GPIO pin = %d \n",
				gpio_pin);
			return -1;
		}
/* TODO: See if the below causes errors */
#if EXTRA_DEBUG
		pr_info("export_gpio_pin: IO PIN = %d, GPIO PIN = %d, io_pin_dir = %d \n",
			io_pin, gpio_pin, io_pin_dir);
#endif
	}
	return 0;
}

static int config_level_shifter(int io_pin, int io_pin_dir)
{
	int ret_val = 0, gpio_pin;
	gpio_pin = gpio_table[io_pin][LEVEL_SHIFTER_PIN];
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		ret_val = gpio_direction_output(gpio_pin, io_pin_dir);
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN direction set failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		/* TODO: See if the below causes errors */
		gpio_set_value_cansleep(gpio_pin, io_pin_dir);
	}
#if EXTRA_DEBUG
	pr_info("config_level_shifter: IO PIN = %d, GPIO PIN = %d, Value(also io_pin_dir) = %d \n",
		io_pin, gpio_pin, io_pin_dir);
#endif

	return 0;
}

static int config_pin_mux1(int io_pin)
{
	int ret_val = 0, gpio_pin;
	gpio_pin = gpio_table[io_pin][PIN_MUX1];
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, pin_mux_table[io_pin][0]);
	}
#if EXTRA_DEBUG
	pr_info("config_pin_mux1: IO PIN = %d, GPIO PIN = %d, Value(also io_pin_dir) = %d \n",
		io_pin, gpio_pin, pin_mux_table[io_pin][0]);
#endif

	return 0;
}

static int config_pin_mux2(int io_pin)
{
	int ret_val = 0, gpio_pin;
	gpio_pin = gpio_table[io_pin][PIN_MUX2];
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, pin_mux_table[io_pin][1]);
	}
#if EXTRA_DEBUG
	pr_info("config_pin_mux2: IO PIN = %d, GPIO PIN = %d, Value(also io_pin_dir) = %d \n",
		io_pin, gpio_pin, pin_mux_table[io_pin][1]);
#endif
	return 0;
}

#if 1
static int configure_gpio_pins(config_pins_t config_pins,
			       htsr04_data_t *dev_datap)
{
	/* Configure trigger pin */
	/* export trigger pin */
	if (config_pins.trigger_pin != -1 && config_pins.echo_pin != -1) {
		export_gpio_pin(config_pins.trigger_pin, OUT_PIN);
		config_level_shifter(config_pins.trigger_pin, OUT_PIN);
		config_pin_mux1(config_pins.trigger_pin);
		config_pin_mux2(config_pins.trigger_pin);

		/*Configure Echo Pin*/
		export_gpio_pin(config_pins.echo_pin, IN_PIN);
		config_level_shifter(config_pins.echo_pin, IN_PIN);
		config_pin_mux1(config_pins.echo_pin);
		config_pin_mux2(config_pins.echo_pin);
		/* Config IRQ for the Echo pin */
		if ((irq_id = gpio_to_irq(
			     gpio_table[config_pins.echo_pin][LINUX_PIN]))
		    < 0) {
			pr_info("IRQ mapping failed for GPIO %d\n", 0);
			return -1;
		}
		if (request_irq(irq_id, &r_irq_handler, IRQF_TRIGGER_RISING,
				GPIO_NAME, dev_datap)) {
			pr_warn("Rising IRQ Request failure\n");
			return -1;
		}
	}
	return 0;
}
#endif

static int release_io_pin(int io_pin)
{
	int i = 0, gpio_pin = -1;
	for (i = 0; i <= 4; i++) {
		gpio_pin = gpio_table[io_pin][i];
		if (gpio_pin != -1) {
			gpio_free(gpio_pin);
		}
	}
	return 0;
}

static int release_config_pins(htsr04_data_t *dev_datap)
{
	pr_info("Freeing echo and trigger gpios ");
	release_io_pin(dev_datap->m_config_pins.echo_pin);
	release_io_pin(dev_datap->m_config_pins.trigger_pin);
	return 0;
}

static int htsr04_open(struct inode *inode, struct file *filp)
{
	int i = 0;
	pr_info("Device open \n");
	for (i = 0; i < n; i++) {
		if (htsr04_datap[i].htsr04_device.minor == iminor(inode)) {
			filp->private_data = &htsr04_datap[i];
			pr_info("The device minor number = %d, device name =  \n",
				i);
			break;
		}
	}
	htsr04_datap[i].m_write_status = NOT_WRITING;
	htsr04_datap[i].cb = kmalloc(sizeof(circular_buffer), GFP_KERNEL);
	cb_init(htsr04_datap[i].cb, CIRCULAR_BUFFER_SIZE,
		sizeof(output_data_t));

	return 0;
}

static int htsr04_close(struct inode *inodep, struct file *filp)
{
	htsr04_data_t *dev_datap = filp->private_data;
	pr_info("Device closing, private data num_samples = %d, sampling_period = %d, echo_pin = %d, trigger_pin = %d ",
		dev_datap->m_sampling_params.num_samples,
		dev_datap->m_sampling_params.sampling_period,
		dev_datap->m_config_pins.echo_pin,
		dev_datap->m_config_pins.trigger_pin);
	free_irq(irq_id, dev_datap);
	release_config_pins(dev_datap);
	return 0;
}

static ssize_t htsr04_write(struct file *filp, const char __user *buf,
			    size_t len, loff_t *ppos)
{
	int *clear_device_measurements;
	htsr04_data_t *dev_datap;
	dev_datap = filp->private_data;
	pr_info("Writing to %s \n", dev_datap->htsr04_device.name);
	clear_device_measurements = kmalloc(sizeof(int), GFP_KERNEL);
	if (!copy_from_user(&clear_device_measurements, buf, len)) {
		pr_info("Write: value passed from user = %d \n",
			*clear_device_measurements);
		/* return -EFAULT; */
	}
	pr_info("Write: value passed from user = %d \n",
		*clear_device_measurements);

	if (clear_device_measurements == 0) {
		/* Dont clear the device buffer
		 * ************************************/
		;
	} else {
		/* clear the device buffer if there is no ongoing
		 * measurement******/
		;
	}

	/* Trigger a new measurement ****************************************/

	trigger_pulse(dev_datap);
	return 0;
}

static ssize_t htsr04_read(struct file *filp, char *buf, size_t count,
			   loff_t *ppos)
{
	htsr04_data_t *dev_datap;
	output_data_t *op_data;
	int x;
	op_data = kmalloc(sizeof(output_data_t), GFP_KERNEL);
	dev_datap = filp->private_data;
	cb_pop_front(dev_datap->cb, op_data);
	x = op_data->distance;
	pr_info("Reading for %s, distance = %d \n",
		dev_datap->htsr04_device.name, x);
	if (copy_to_user((int *)buf, &(op_data->distance), sizeof(int))) {
		pr_warn("copy_to_user failed");
		return -EFAULT;
	}
	return 0;
}

static long htsr04_ioctl(struct file *filp, unsigned int ioctl_num,
			 unsigned long ioctl_params)
{
	sampling_params_t m_sampling_params_local;
	config_pins_t m_config_pins_local;
	htsr04_data_t *dev_datap;
	dev_datap = filp->private_data;
	pr_info("Configuring GPIO for %s ", dev_datap->htsr04_device.name);


	pr_info("Reading ioctl \n");

	switch (ioctl_num) {
	case SET_PARAMETERS: {
		copy_from_user(&m_sampling_params_local,
			       (sampling_params_t *)ioctl_params,
			       sizeof(sampling_params_t));
		if (dev_datap->m_write_status == NOT_WRITING) {
			dev_datap->distances_mm = kmalloc(
				sizeof(int)
					* (m_sampling_params_local.num_samples
					   + 2),
				GFP_KERNEL);
			dev_datap->distances_count = 0;
		} else if (dev_datap->m_write_status == WRITING) {
			/* Write is going on, abort the IOCTL write*/
			pr_info("WRITE is going on, cancelling the IOCTL operation");
			return -1;
		} else if (dev_datap->m_write_status == INVALID_STATUS) {
			pr_info("Write is going on, aborting IOCTL set operation");
			return -1;
		}
		pr_info("SET_PARAMETERS ioctl invoked, num_samples = %d, sampling_period = %d \n",
			m_sampling_params_local.num_samples,
			m_sampling_params_local.sampling_period);
		memcpy(&dev_datap->m_sampling_params, &m_sampling_params_local,
		       sizeof(sampling_params_t));


		break;
	}
	case CONFIG_PINS: {
		if (dev_datap->m_write_status == NOT_WRITING) {
			copy_from_user(&m_config_pins_local,
				       (config_pins_t *)ioctl_params,
				       sizeof(config_pins_t));
			pr_info("CONFIG_PINS ioctl invoked, trigger_pin = %d, echo_pin = %d \n",
				m_config_pins_local.trigger_pin,
				m_config_pins_local.echo_pin);
			/* TODO: put a check to see if the pins are valid */
			memcpy(&(dev_datap->m_config_pins),
			       &m_config_pins_local, sizeof(config_pins_t));
			/* TODO: unset previously configured pins */
			/* TODO: move below code to another function */
			configure_gpio_pins(m_config_pins_local, dev_datap);
		} else if (dev_datap->m_write_status == WRITING) {
			pr_info("Write is going on, aborting IOCTL set operation");
			return -1;
		} else if (dev_datap->m_write_status == INVALID_STATUS) {
			pr_info("Write is going on, aborting IOCTL set operation");
			return -1;
		}
		break;
	}
	default:
		pr_warn("Invalid ioctl set");
		break;
	}
	return 0;
}

static const struct file_operations htsr04_fops = {
	.owner = THIS_MODULE,
	.write = htsr04_write,
	.open = htsr04_open,
	.release = htsr04_close,
	.llseek = no_llseek,
	.read = htsr04_read,
	.unlocked_ioctl = htsr04_ioctl,
};

#if 0
static int __init htsr04_init(void)
{
	int error, i = 0;
	char *temp_name;
	struct miscdevice htsr04_dev_temp = {
		.minor = MISC_DYNAMIC_MINOR,
		.name = "HCSR_n",
		.fops = &htsr04_fops,
	};
	htsr04_datap = kmalloc(n * sizeof(htsr04_data_t), GFP_KERNEL);
	pr_info("Number of devices = %d \n", n);
	for (i = 0; i < n; i++) {
		temp_name = kmalloc(BUFSIZE, GFP_KERNEL);
		snprintf(temp_name, BUFSIZE, "HCSR_%d", i);
		pr_info("name = %s", temp_name);
		memcpy(&htsr04_datap[i].htsr04_device, &htsr04_dev_temp,
		       sizeof(struct miscdevice));
		htsr04_datap[i].htsr04_device.name = temp_name;
		pr_info("name = %s", htsr04_datap[i].htsr04_device.name);
		error = misc_register(&htsr04_datap[i].htsr04_device);
		if (error) {
			pr_err("can't init misc_register :(\n");
			return error;
		}
	}
	pr_info("Module Initialised \n");
	return 0;
}
#endif
/* static void __exit htsr04_exit(void) */
/* { */
/* 	int i = 0; */
/* 	for (i = 0; i < n; i++) { */
/* 		misc_deregister(&htsr04_datap[i].htsr04_device); */
/* 	} */
/* 	pr_info("Device exit\n"); */
/* 	/\* TODO: Make the generic *\/ */
/* 	//  free_irq(gpio_to_irq(m_config_pins.echo_pin), NULL); */
/* } */

static ssize_t trigger_pin_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	return snprintf(buf, PAGE_SIZE, "%d\n",
			p_driver_data->m_config_pins.trigger_pin);
}

static ssize_t echo_pin_show(struct device *dev, struct device_attribute *attr,
			     char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	return snprintf(buf, PAGE_SIZE, "%d\n",
			p_driver_data->m_config_pins.trigger_pin);
}
static ssize_t number_samples_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	return snprintf(buf, PAGE_SIZE, "%d\n",
			p_driver_data->m_sampling_params.num_samples);
}
static ssize_t sampling_period_show(struct device *dev,
				    struct device_attribute *attr, char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	return snprintf(buf, PAGE_SIZE, "%d\n",
			p_driver_data->m_sampling_params.sampling_period);
}
static ssize_t enable_show(struct device *dev, struct device_attribute *attr,
			   char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	return snprintf(buf, PAGE_SIZE, "%d\n", p_driver_data->enable);
}
static ssize_t distance_show(struct device *dev, struct device_attribute *attr,
			     char *buf)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	int op_data;
	mutex_lock(&distance_lock);
	op_data = p_driver_data->recent_distance;
	mutex_unlock(&distance_lock);

	return snprintf(buf, PAGE_SIZE, "%d\n", op_data);
}

static ssize_t trigger_pin_store(struct device *dev,
				 struct device_attribute *attr, const char *buf,
				 size_t count)
{
	/* TODO: Not permitted when an operation is going on */
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	sscanf(buf, "%d", &p_driver_data->m_config_pins.trigger_pin);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	/* TODO: /invoke the config pins function. Init both to -1 in main
	 * function to -1 */
	/* TODO: Free the pins being used currently */
	configure_gpio_pins(p_driver_data->m_config_pins, p_driver_data);
	//
	return count;
}

static ssize_t echo_pin_store(struct device *dev, struct device_attribute *attr,
			      const char *buf, size_t count)
{
	/* TODO: Not permitted when an operation is going on */
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	sscanf(buf, "%d", &p_driver_data->m_config_pins.echo_pin);
	/* TODO: Free the pins being used currently */
	configure_gpio_pins(p_driver_data->m_config_pins, p_driver_data);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	return count;
}

static ssize_t number_samples_store(struct device *dev,
				    struct device_attribute *attr,
				    const char *buf, size_t count)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	sscanf(buf, "%d", &p_driver_data->m_sampling_params.num_samples);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	p_driver_data->distances_mm = kmalloc(
		sizeof(int)
			* (p_driver_data->m_sampling_params.num_samples + 2),
		GFP_KERNEL);
	p_driver_data->distances_count = 0;

	return count;
}

static ssize_t sampling_period_store(struct device *dev,
				     struct device_attribute *attr,
				     const char *buf, size_t count)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	sscanf(buf, "%d", &p_driver_data->m_sampling_params.sampling_period);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	return count;
}

static ssize_t enable_store(struct device *dev, struct device_attribute *attr,
			    const char *buf, size_t count)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	mutex_lock(&enable_lock);
	sscanf(buf, "%d", &p_driver_data->enable);
	mutex_unlock(&enable_lock);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	// Start writing
	if (p_driver_data->enable == 1) {
		trigger_pulse(p_driver_data);
	}

	return count;
}


static ssize_t distance_store(struct device *dev, struct device_attribute *attr,
			      const char *buf, size_t count)
{
	htsr04_data_t *p_driver_data = dev_get_drvdata(dev);
	sscanf(buf, "%d", &p_driver_data->trigger_pin);
	//	configure(pshcsr_dev_obj->trigger_pin, 0, 0);
	return count;
}

static DEVICE_ATTR(trigger, S_IRWXU, trigger_pin_show, trigger_pin_store);
static DEVICE_ATTR(echo, S_IRWXU, echo_pin_show, echo_pin_store);
static DEVICE_ATTR(number_samples, S_IRWXU, number_samples_show,
		   number_samples_store);
static DEVICE_ATTR(sampling_period, S_IRWXU, sampling_period_show,
		   sampling_period_store);
static DEVICE_ATTR(enable, S_IRWXU, enable_show, enable_store);
static DEVICE_ATTR(distance, S_IRWXU, distance_show, distance_store);

htsr04_data_t *init_pf_device(void *p_chip_handle)
{
	int ret;
	hcsr04_device_t *p_device_chip;
	htsr04_data_t *p_driver_data;
	p_device_chip = (hcsr04_device_t *)p_chip_handle;

	p_driver_data = kmalloc(sizeof(htsr04_data_t), GFP_KERNEL);
	pr_info("[init_device] device kmalloc buf\n");
	if (!p_driver_data) {
		pr_warn("[init_device] Bad Kmalloc\n");
	}
	memset(p_driver_data, 0, sizeof(htsr04_data_t));
	p_driver_data->hcsr_dev_p = p_device_chip;

	p_driver_data->m_config_pins.echo_pin = -1;
	p_driver_data->m_config_pins.trigger_pin = -1;

	if (p_device_chip->dev_no == 1) {
		pr_info("[init_device] dev_no = 1");
		p_driver_data->dev_name =
			kmalloc(sizeof(DEVICE_NAME1) + 1, GFP_KERNEL);
		snprintf(p_driver_data->dev_name, sizeof(DEVICE_NAME1),
			 DEVICE_NAME1);
		pr_info("Device name = %s", p_driver_data->dev_name);
		p_driver_data->htsr04_device.name = DEVICE_NAME1;
	} else {
		pr_info("[init_device] The dev_no is not 1");
	}

	pr_info("Registering minor number");
	p_driver_data->htsr04_device.minor = minor_num_base++;
	pr_info("Adding file operations");
	p_driver_data->htsr04_device.fops = &htsr04_fops;
	pr_info("registering misc device");
	ret = misc_register(&p_driver_data->htsr04_device);

	p_driver_data->m_write_status = NOT_WRITING;
	p_driver_data->cb = kmalloc(sizeof(circular_buffer), GFP_KERNEL);
	cb_init(p_driver_data->cb, CIRCULAR_BUFFER_SIZE, sizeof(output_data_t));

	p_driver_data->distances_count = 0;


	pr_info("init_pf_device done");
	return p_driver_data;
}


static int pf_driver_probe(struct platform_device *pf_dev_found)
{
	int ret_val = 0;
	htsr04_data_t *p_driver_data;
	hcsr04_device_t *p_device_chip;

	p_device_chip = container_of(pf_dev_found, hcsr04_device_t, pf_dev);
	pr_info("Found device %s %d", p_device_chip->name,
		p_device_chip->dev_no);

	if (m_create_class == CREATE_CLASS) {
		p_hcsr_class = class_create(THIS_MODULE, CLASS_NAME);
		if (IS_ERR(p_hcsr_class)) {
			pr_warn("Class creation failed");
		}
		m_create_class = CLASS_CREATED;
	}
	if (!(p_driver_data = init_pf_device(p_device_chip))) {
		pr_info("Device init failed");
	}
	pr_info("test print p_driver_data->name");
	pr_info("p_driver_data->name = %s", p_driver_data->dev_name);
	p_hcsr_device = device_create(
		p_hcsr_class, NULL, pf_devt, p_driver_data,
		p_device_chip->name); /* should be p_driver_data->name*/
	if (IS_ERR(p_hcsr_device)) {
		pr_info(" Device  %s cant be created\n",
			p_driver_data->dev_name);
	}
	INIT_LIST_HEAD(&p_driver_data->device_entry);
	list_add(&p_driver_data->device_entry, &device_list);

	pr_info("Device create succeeded");

	ret_val = device_create_file(p_hcsr_device, &dev_attr_trigger);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name, dev_attr_trigger.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	ret_val = device_create_file(p_hcsr_device, &dev_attr_echo);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name, dev_attr_echo.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	ret_val = device_create_file(p_hcsr_device, &dev_attr_number_samples);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name,
		       dev_attr_number_samples.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	ret_val = device_create_file(p_hcsr_device, &dev_attr_sampling_period);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name,
		       dev_attr_sampling_period.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	ret_val = device_create_file(p_hcsr_device, &dev_attr_enable);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name, dev_attr_enable.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	ret_val = device_create_file(p_hcsr_device, &dev_attr_distance);
	if (ret_val < 0) {
		printk("Cant create device attribute %s %s\n",
		       p_driver_data->dev_name, dev_attr_distance.attr.name);
	} else {
		pr_info("device_create_file done");
	}
	return 0;
}

static int pf_driver_remove(struct platform_device *pdev)
{
	htsr04_data_t *p_driver_data;
	pr_info("pf_driver_remove invoked");
	list_for_each_entry(p_driver_data, &device_list, device_entry)
	{

		if (p_driver_data->dev_name == pdev->name) {
			list_del(&p_driver_data->device_entry);
			device_destroy(p_hcsr_class,
				       p_driver_data->htsr04_device.minor);
			misc_deregister(&p_driver_data->htsr04_device);
			kfree(p_driver_data);
			break;
		}
	}
	class_unregister(p_hcsr_class);
	class_destroy(p_hcsr_class);
	return 0;
}


static struct platform_driver pf_driver = {
	.driver =
		{
			.name = DRIVER_NAME,
			.owner = THIS_MODULE,
		},
	.probe = pf_driver_probe,
	.remove = pf_driver_remove,
	.id_table = pdev_id_table,
};

MODULE_DESCRIPTION("HC-SR04 distance Sensor Driver");
MODULE_AUTHOR("Sree Gowtham Josyula");
MODULE_LICENSE("GPL");

module_platform_driver(pf_driver);
